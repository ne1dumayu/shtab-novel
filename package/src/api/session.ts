import axios from 'axios';

const session = axios.create({
  baseURL: 'https://dev.my.shtab.app/api',
  headers: {
    // TODO use token from store - collision with getTask request
    Authorization: 'Token 63d8fa4a7777dcc0bc152c3807b58f440cbea90b0ab1f3207f22a6a2ac06eed7',
  },
});

session.interceptors.response.use(
  (response) => response,
  (error) => {
    console.error(error);
  },
);

session.interceptors.request.use((config) => {
  config.baseURL = 'https://dev.my.shtab.app/api';
  return config;
});

export default session;
