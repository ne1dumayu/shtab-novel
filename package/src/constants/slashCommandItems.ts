import { startImageUpload } from '@/extensions/uploadImages';
import { Editor, Range } from '@tiptap/core';
import { startFileUpload } from '@/extensions/uploadFile';

interface CommandProps {
  editor: Editor;
  range: Range;
}

export const slashCommandItems = [
  {
    title: 'Heading 1',
    description: 'Big section heading.',
    searchTerms: ['title', 'big', 'large'],
    icon: 'h-1',
    command: ({ editor, range }: CommandProps) => {
      editor.chain().focus().deleteRange(range).setNode('heading', { level: 1 }).run();
    },
  },
  {
    title: 'Heading 2',
    description: 'Medium section heading.',
    searchTerms: ['subtitle', 'medium'],
    icon: 'h-2',
    command: ({ editor, range }: CommandProps) => {
      editor.chain().focus().deleteRange(range).setNode('heading', { level: 2 }).run();
    },
  },
  {
    title: 'Heading 3',
    description: 'Small section heading.',
    searchTerms: ['subtitle', 'small'],
    icon: 'h-3',
    command: ({ editor, range }: CommandProps) => {
      editor.chain().focus().deleteRange(range).setNode('heading', { level: 3 }).run();
    },
  },
  {
    title: 'Text',
    description: 'Just start typing with plain text.',
    searchTerms: ['p', 'paragraph'],
    icon: 'text',
    command: ({ editor, range }: CommandProps) => {
      editor.chain().focus().deleteRange(range).toggleNode('paragraph', 'paragraph').run();
    },
  },
  {
    title: 'Numbered List',
    description: 'Create a list with numbering.',
    searchTerms: ['ordered'],
    icon: 'list-ordered',
    command: ({ editor, range }: CommandProps) => {
      editor.chain().focus().deleteRange(range).toggleOrderedList().run();
    },
  },
  {
    title: 'Bullet List',
    description: 'Create a simple bullet list.',
    searchTerms: ['unordered', 'point'],
    icon: 'list-unordered',
    command: ({ editor, range }: CommandProps) => {
      editor.chain().focus().deleteRange(range).toggleBulletList().run();
    },
  },
  {
    title: 'Quote',
    description: 'Capture a quote.',
    searchTerms: ['blockquote'],
    icon: 'quote-text',
    command: ({ editor, range }: CommandProps) =>
      editor.chain().focus().deleteRange(range).toggleNode('paragraph', 'paragraph').toggleBlockquote().run(),
  },
  {
    title: 'File',
    description: 'Add a file',
    searchTerms: ['file'],
    icon: 'attachment',
    command: ({ editor, range }: CommandProps) => {
      editor.chain().focus().deleteRange(range).run();
      const input = document.createElement('input');
      input.type = 'file';
      input.accept = '*';
      input.onchange = async () => {
        if (input.files?.length) {
          const file = input.files[0];
          const pos = editor.view.state.selection.from;
          startFileUpload(file, editor.view, pos);
        }
      };
      input.click();
    },
  },
  {
    title: 'To-do List',
    description: 'Track tasks with a to-do list.',
    searchTerms: ['todo', 'task', 'list', 'check', 'checkbox'],
    icon: 'checklist',
    command: ({ editor, range }: CommandProps) => {
      editor.chain().focus().deleteRange(range).toggleTaskList().run();
    },
  },
  {
    title: 'Table',
    description: 'Create table',
    searchTerms: ['table'],
    icon: 'table',
    command: ({ editor, range }: CommandProps) => {
      editor.chain().focus().deleteRange(range).insertTable().run();
    },
  },
  {
    title: 'Callout',
    description: 'Create a callout',
    searchTerms: ['callout'],
    icon: 'callout',
    command: ({ editor, range }: CommandProps) => {
      editor.chain().focus().deleteRange(range).addCallout().run();
    },
  },
  {
    title: 'Hidden text',
    description: 'Hide all your secrets',
    searchTerms: ['hidden', 'hidden text'],
    icon: 'hidden-text',
    command: ({ editor, range }: CommandProps) => {
      editor.chain().focus().deleteRange(range).addHiddenText().run();
    },
  },
  {
    title: 'Split',
    description: 'Enter split to decompose your text',
    searchTerms: ['split'],
    icon: 'split',
    command: ({ editor, range }: CommandProps) => {
      editor.chain().focus().deleteRange(range).setHorizontalRule().run();
    },
  },
  {
    title: '2 columns',
    description: 'Split your data into columns',
    searchTerms: ['columns'],
    icon: 'columns',
    command: ({ editor, range }: CommandProps) => {
      editor.chain().focus().deleteRange(range).setColumns(2).run();
    },
  },
  {
    title: '3 columns',
    description: 'Split your data into columns',
    searchTerms: ['columns'],
    icon: 'columns',
    command: ({ editor, range }: CommandProps) => {
      editor.chain().focus().deleteRange(range).setColumns(3).run();
    },
  },
  {
    title: '4 columns',
    description: 'Split your data into columns',
    searchTerms: ['columns'],
    icon: 'columns',
    command: ({ editor, range }: CommandProps) => {
      editor.chain().focus().deleteRange(range).setColumns(4).run();
    },
  },
  {
    title: '5 columns',
    description: 'Split your data into columns',
    searchTerms: ['columns'],
    icon: 'columns',
    command: ({ editor, range }: CommandProps) => {
      editor.chain().focus().deleteRange(range).setColumns(5).run();
    },
  },
  {
    title: 'Embed',
    description: 'Embed anything you want',
    searchTerms: ['embed', 'iframe'],
    icon: 'embeded',
    command: ({ editor, range }: CommandProps) => {
      editor.chain().focus().deleteRange(range).setIframeLink().run();
    },
  },
  {
    title: 'Embed view',
    description: 'Embed view',
    searchTerms: ['embed'],
    icon: 'embeded-view',
    command: ({ editor, range }: CommandProps) => {
      //TODO select of views
      editor.chain().focus().deleteRange(range).setIframeLink().run();
    },
  },
  {
    title: 'Image',
    description: 'Upload an image from your computer.',
    searchTerms: ['photo', 'picture', 'media'],
    icon: 'image',
    command: ({ editor, range }: CommandProps) => {
      editor.chain().focus().deleteRange(range).run();
      const input = document.createElement('input');
      input.type = 'file';
      input.accept = 'image/*';
      input.onchange = async () => {
        if (input.files?.length) {
          const file = input.files[0];
          const pos = editor.view.state.selection.from;
          startImageUpload(file, editor.view, pos);
        }
      };
      input.click();
    },
  },
  {
    title: 'Code',
    description: 'Capture a code snippet.',
    searchTerms: ['codeblock', 'code'],
    icon: 'code',
    command: ({ editor, range }: CommandProps) =>
      editor.chain().focus().deleteRange(range).toggleCodeBlock({ language: 'javascript' }).run(),
  },
];
