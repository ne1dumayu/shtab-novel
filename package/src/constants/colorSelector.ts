export const TEXT_COLORS = [
  {
    name: 'Default',
    color: '#171e3a',
  },
  {
    name: 'Purple',
    color: '#9333EA',
  },
  {
    name: 'Red',
    color: '#E00000',
  },
  {
    name: 'Yellow',
    color: '#EAB308',
  },
  {
    name: 'Blue',
    color: '#2563EB',
  },
  {
    name: 'Green',
    color: '#008A00',
  },
  {
    name: 'Orange',
    color: '#FFA500',
  },
  {
    name: 'Pink',
    color: '#BA4081',
  },
  {
    name: 'Gray',
    color: '#A8A29E',
  },
];

export const HIGHLIGHT_COLORS = [
  {
    name: 'Default',
    color: '#ffffff',
  },
  {
    name: 'Purple',
    color: '#f6f3f8',
  },
  {
    name: 'Red',
    color: '#fdebeb',
  },
  {
    name: 'Yellow',
    color: '#fbf4a2',
  },
  {
    name: 'Blue',
    color: '#c1ecf9',
  },
  {
    name: 'Green',
    color: '#acf79f',
  },
  {
    name: 'Orange',
    color: '#faebdd',
  },
  {
    name: 'Pink',
    color: '#faf1f5',
  },
  {
    name: 'Gray',
    color: '#f1f1ef',
  },
];
