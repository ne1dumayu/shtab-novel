import { mergeAttributes, Node } from '@tiptap/core';
import { VueNodeViewRenderer } from '@tiptap/vue-3';
import MentionItem from '@/components/MentionItem.vue';

declare module '@tiptap/core' {
  interface Commands<ReturnType> {
    mentionItem: {
      setItemMention: (options: any) => ReturnType;
    };
  }
}

export const UserMentionMark = Node.create({
  name: 'mentionItem',

  group: 'inline',

  inline: true,

  atom: true,

  addAttributes() {
    return {
      id: {
        default: null,
        parseHTML: (element) => element.getAttribute('data-id'),
        renderHTML: (attributes) => {
          return {
            'data-id': attributes.id,
          };
        },
      },

      label: {
        default: null,
        parseHTML: (element) => element.getAttribute('data-label'),
        renderHTML: (attributes) => {
          return {
            'data-label': attributes.label,
          };
        },
      },

      href: {
        default: null,
        parseHTML: (element) => element.getAttribute('href'),
        renderHTML: (attributes) => {
          return {
            href: attributes.href,
          };
        },
      },
    };
  },

  parseHTML() {
    return [
      {
        tag: `mention-item`,
      },
    ];
  },

  renderHTML({ HTMLAttributes }) {
    return ['mention-item', mergeAttributes(HTMLAttributes)];
  },

  addCommands() {
    return {
      setItemMention:
        (options) =>
        ({ commands }) => {
          return commands.insertContent({
            type: this.name,
            attrs: options,
          });
        },
    };
  },

  addNodeView() {
    return VueNodeViewRenderer(MentionItem);
  },
});
