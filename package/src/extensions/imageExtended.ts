import TiptapImage from '@tiptap/extension-image';
import UploadImagesPlugin from './uploadImages';
import { VueNodeViewRenderer } from '@tiptap/vue-3';
import ResizableImageTemplate from '@/components/ResizableImageTemplate.vue';

export const ImageExtended = TiptapImage.extend({
  addProseMirrorPlugins() {
    return [UploadImagesPlugin()];
  },
  addAttributes() {
    return {
      ...this.parent?.(),
      width: {
        default: 'auto',
        renderHTML: (attributes) => {
          return {
            width: attributes.width,
          };
        },
      },
      height: {
        default: 'auto',
        renderHTML: (attributes) => {
          return {
            height: attributes.height,
          };
        },
      },
      isFigcaption: {
        default: false,
        renderHTML: (attributes) => {
          return {
            isFigcaption: attributes.isFigcaption,
          };
        },
      },
      figcaption: {
        default: '',
        renderHTML: (attributes) => {
          return {
            figcaption: attributes.figcaption,
          };
        },
      },
      align: {
        default: 'left',
        renderHTML: (attributes) => {
          return {
            align: attributes.align,
          };
        },
      },
      alt: {
        default: '',
        renderHTML: (attributes) => {
          return {
            alt: attributes.alt,
          };
        },
      },
    };
  },
  addCommands() {
    return {
      ...this.parent?.(),

      editImage:
        () =>
        ({ tr }: any) => {
          const { node } = tr?.selection;
          if (node?.type?.name === 'image') {
            //do something
          }
        },
    };
  },
  addNodeView() {
    return VueNodeViewRenderer(ResizableImageTemplate);
  },
}).configure({
  allowBase64: true,
});
