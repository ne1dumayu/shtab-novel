import { DecorationSet, EditorView } from '@tiptap/pm/view';
import { userStore } from '@/store/store';
import axios from 'axios';
import session from '@/api/session';
import { Plugin, PluginKey } from '@tiptap/pm/state';

const uploadKey = new PluginKey('upload-file-key');

const UploadFilePlugin = () => {
  return new Plugin({
    key: uploadKey,
    state: {
      init() {
        return DecorationSet.empty;
      },
      apply(tr, set) {
        set = set.map(tr.mapping, tr.doc);
        return set;
      },
    },
    props: {
      decorations(state) {
        return this.getState(state);
      },
    },
  });
};

export default UploadFilePlugin;

export const startFileUpload = (file: File, view: EditorView, pos: number) => {
  const tr = view.state.tr;
  if (!tr.selection.empty) tr.deleteSelection();

  const { userProfile } = userStore();
  const fd = new FormData();
  fd.append('file', file);
  fd.append('task', String(userProfile.taskId));
  const cancelToken = axios.CancelToken;
  const source = cancelToken.source();
  // @ts-ignore
  source.fileSource = 'description';
  const config = {
    name: file.name,
    cancelToken: source.token,
  };
  const requestData = {
    task: userProfile.taskId,
    fd,
    config,
  };

  handleFileUpload(requestData).then((data: any) => {
    const { schema } = view.state;
    const node = schema.nodes.fileUploaded.create({ file: data });
    const transaction = view.state.tr.replaceWith(pos - 1, pos, node);
    view.dispatch(transaction);
  });
};

const handleFileUpload = ({ task, fd, config }: any) => {
  return new Promise((resolve, reject) => {
    session
      .post(`tasks/files/${task}/create/`, fd, config)
      .then(async (response) => {
        if (response.status === 201) {
          const file = response.data.file[0];
          resolve(file);
        } else if (response.status === 401) {
          throw new Error('`BLOB_READ_WRITE_TOKEN` environment variable not found, reading image locally instead.');
        } else {
          reject(new Error(`Error uploading image. Please try again.`));
        }
      })
      .catch((error) => {
        reject(new Error(`Error uploading image: ${error.message}`));
      });
  });
};
