import { mergeAttributes, Node } from '@tiptap/core';
import { VueNodeViewRenderer } from '@tiptap/vue-3';
import HiddenText from '@/components/HiddenText.vue';

declare module '@tiptap/core' {
  interface Commands<ReturnType> {
    hiddenText: {
      addHiddenText: (options?: any) => ReturnType;
    };
  }
}

export const HiddenTextNode = Node.create({
  name: 'hiddenText',

  group: 'block',

  content: 'block*',

  draggable: true,

  parseHTML() {
    return [
      {
        tag: 'hidden-text',
      },
    ];
  },
  renderHTML({ HTMLAttributes }) {
    return ['hidden-text', mergeAttributes(HTMLAttributes), 0];
  },
  addAttributes() {
    return {
      title: {
        default: '',
        renderHTML: (attributes) => {
          return {
            title: attributes.title,
          };
        },
      },
    };
  },
  addNodeView() {
    return VueNodeViewRenderer(HiddenText);
  },
  addCommands() {
    return {
      addHiddenText:
        (options) =>
        ({ commands }) => {
          return commands.insertContent({
            type: this.name,
            attrs: options,
            content: [
              {
                type: 'paragraph',
              },
            ],
          });
        },
    };
  },
});
