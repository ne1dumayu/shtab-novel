import StarterKit from '@tiptap/starter-kit';
import HorizontalRule from '@tiptap/extension-horizontal-rule';
import TiptapLink from '@tiptap/extension-link';
import Placeholder from '@tiptap/extension-placeholder';
import TiptapUnderline from '@tiptap/extension-underline';
import TextStyle from '@tiptap/extension-text-style';
import { Color } from '@tiptap/extension-color';
import TaskItem from '@tiptap/extension-task-item';
import TaskList from '@tiptap/extension-task-list';
import { Markdown } from 'tiptap-markdown';
import Highlight from '@tiptap/extension-highlight';
import { CodeBlockLowlight } from '@tiptap/extension-code-block-lowlight';
import { InputRule } from '@tiptap/core';
import SlashCommand from './slashExtension';
import UserSuggestion from './mentionUser';
import DragAndDrop from './dragAndDrop';
import { Table } from '@tiptap/extension-table';
import { TableHeader } from '@tiptap/extension-table-header';
import { TableRow } from '@tiptap/extension-table-row';
import { common, createLowlight } from 'lowlight';
import { ImageExtended } from './imageExtended';
import { FileUploadNode } from './fileUploadNode';
import { CalloutNode } from './calloutNode';
import { HardBreak } from '@tiptap/extension-hard-break';
import { HiddenTextNode } from './hiddenTextNode';
import { IframeNode } from './iframeNode';
import { CustomTableCell } from './customTableCell';
import ColumnExtension from './columns/columnExtension';
import { UserMentionMark } from './userMentionMark';

const lowlight = createLowlight(common);

export const defaultExtensions = [
  StarterKit.configure({
    horizontalRule: false,
    dropcursor: {
      color: '#e3e8fc',
      width: 4,
    },
    gapcursor: false,
  }),
  CodeBlockLowlight.configure({
    lowlight,
  }),
  HorizontalRule.extend({
    addInputRules() {
      return [
        new InputRule({
          find: /^(?:---|—-|___\s|\*\*\*\s)$/,
          handler: ({ state, range }) => {
            const attributes = {};

            const { tr } = state;
            const start = range.from;
            let end = range.to;

            tr.insert(start - 1, this.type.create(attributes)).delete(tr.mapping.map(start), tr.mapping.map(end));
          },
        }),
      ];
    },
  }),
  TiptapLink,
  Placeholder.configure({
    placeholder: ({ node }) => {
      if (node.type.name === 'heading') {
        return `Heading ${node.attrs.level}`;
      } else if (node.type.name === 'callout') {
        return '';
      }
      return 'Start typing or use / command';
    },
    includeChildren: true,
  }),
  TiptapUnderline,
  TextStyle,
  Table.configure({
    resizable: true,
    allowTableNodeSelection: true,
  }),
  CustomTableCell,
  TableHeader,
  TableRow,
  Color,
  Highlight.configure({
    multicolor: true,
  }),
  HardBreak.configure({
    keepMarks: false,
  }),
  TaskList,
  TaskItem.configure({
    nested: true,
  }),
  Markdown.configure({
    html: false,
    transformCopiedText: true,
  }),
  SlashCommand,
  UserSuggestion,
  DragAndDrop,
  ImageExtended,
  FileUploadNode,
  CalloutNode,
  HiddenTextNode,
  IframeNode,
  ColumnExtension,
  UserMentionMark,
];
