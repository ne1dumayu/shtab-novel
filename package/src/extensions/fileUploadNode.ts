import { mergeAttributes, Node } from '@tiptap/core';
import UploadFilePlugin from './uploadFile';
import { VueNodeViewRenderer } from '@tiptap/vue-3';
import FileUploaded from '@/components/FileUploaded.vue';

export const FileUploadNode = Node.create({
  addProseMirrorPlugins() {
    return [UploadFilePlugin()];
  },
  name: 'fileUploaded',
  group: 'block',
  atom: true,

  draggable: true,
  parseHTML() {
    return [
      {
        tag: 'file-upload',
      },
    ];
  },
  renderHTML({ HTMLAttributes }) {
    return ['file-upload', mergeAttributes(HTMLAttributes)];
  },
  addAttributes() {
    return {
      file: {
        default: null,
        renderHTML: (attributes) => {
          return {
            file: attributes.file,
          };
        },
      },
    };
  },
  addNodeView() {
    return VueNodeViewRenderer(FileUploaded);
  },
});
