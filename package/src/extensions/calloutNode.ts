import { Node, mergeAttributes } from '@tiptap/core';
import { VueNodeViewRenderer } from '@tiptap/vue-3';
import Callout from '@/components/Callout.vue';

declare module '@tiptap/core' {
  interface Commands<ReturnType> {
    callout: {
      addCallout: (options?: any) => ReturnType;
    };
  }
}

export const CalloutNode = Node.create({
  name: 'callout',

  group: 'block',

  content: 'inline*',

  draggable: true,

  parseHTML() {
    return [
      {
        tag: 'callout',
      },
    ];
  },
  renderHTML({ HTMLAttributes }) {
    return ['callout', mergeAttributes(HTMLAttributes), 0];
  },

  addAttributes() {
    return {
      icon: {
        default: '💡',
        renderHTML: (attributes) => {
          return {
            icon: attributes.icon,
          };
        },
      },
    };
  },
  addNodeView() {
    return VueNodeViewRenderer(Callout);
  },
  addCommands() {
    return {
      addCallout:
        (options) =>
        ({ commands }) => {
          return commands.insertContent({
            type: this.name,
            attrs: options,
          });
        },
    };
  },
});
