import { EditorState, Plugin, PluginKey } from '@tiptap/pm/state';
import { Decoration, DecorationSet, EditorView } from '@tiptap/pm/view';
import { userStore } from '@/store/store';
import axios from 'axios';
import session from '@/api/session';

const uploadKey = new PluginKey('upload-image');

const UploadImagesPlugin = () =>
  new Plugin({
    key: uploadKey,
    state: {
      init() {
        return DecorationSet.empty;
      },
      apply(tr, set) {
        set = set.map(tr.mapping, tr.doc);

        const action = tr.getMeta(this as any);
        if (action && action.add) {
          const { id, pos, src } = action.add;

          const placeholder = document.createElement('div');
          placeholder.setAttribute('class', 'img-placeholder');
          const image = document.createElement('img');
          image.setAttribute('class', 'image-uploaded');
          image.src = src;
          placeholder.appendChild(image);
          const deco = Decoration.widget(pos + 1, placeholder, {
            id,
          });
          set = set.add(tr.doc, [deco]);
        } else if (action && action.remove) {
          set = set.remove(set.find(undefined, undefined, (spec) => spec.id == action.remove.id));
        }
        return set;
      },
    },
    props: {
      decorations(state) {
        return this.getState(state);
      },
    },
  });

export default UploadImagesPlugin;

function findPlaceholder(state: EditorState, id: {}) {
  const decos = uploadKey.getState(state);
  const found = decos.find(null, null, (spec: any) => spec.id == id);
  return found.length ? found[0].from : null;
}

export function startImageUpload(file: File, view: EditorView, pos: number) {
  const id = {};

  const tr = view.state.tr;
  if (!tr.selection.empty) tr.deleteSelection();

  const { userProfile } = userStore();
  const fd = new FormData();
  fd.append('file', file);
  fd.append('task', String(userProfile.taskId));
  const cancelToken = axios.CancelToken;
  const source = cancelToken.source();
  // @ts-ignore
  source.fileSource = 'description';
  const config = {
    name: file.name,
    cancelToken: source.token,
  };
  const requestData = {
    task: userProfile.taskId,
    fd,
    config,
  };
  const reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = () => {
    tr.setMeta(uploadKey, { add: { id, pos, src: reader.result } });
    view.dispatch(tr);
  };

  handleImageUpload(requestData).then((src) => {
    const { schema } = view.state;

    let pos = findPlaceholder(view.state, id);
    if (pos == null) return;
    const imageSrc = typeof src === 'object' ? reader.result : src;

    const node = schema.nodes.image.create({ src: imageSrc });
    const transaction = view.state.tr.replaceWith(pos, pos, node).setMeta(uploadKey, { remove: { id } });
    view.dispatch(transaction);
  });
}

const handleImageUpload = ({ task, fd, config }: any) => {
  return new Promise((resolve, reject) => {
    session
      .post(`tasks/files/${task}/create/`, fd, config)
      .then(async (response) => {
        if (response.status === 201) {
          const url = response.data.file[0].file;

          let image = new Image();
          image.src = url;

          image.onload = () => {
            resolve(url);
          };
        } else if (response.status === 401) {
          throw new Error('`BLOB_READ_WRITE_TOKEN` environment variable not found, reading image locally instead.');
        } else {
          reject(new Error(`Error uploading image. Please try again.`));
        }
      })
      .catch((error) => {
        reject(new Error(`Error uploading image: ${error.message}`));
      });
  });
};
