import { Ref } from 'vue';

export const validKeyCodes = [
  ...[...Array(58 - 48).keys()].map((i) => i + 48),
  ...[...Array(91 - 65).keys()].map((i) => i + 65),
  ...[...Array(193 - 186).keys()].map((i) => i + 186),
  ...[...Array(222 - 219).keys()].map((i) => i + 219),
];

type UseMentionItem = {
  search: Ref<string>;
  props: any;
  items: Ref<any[]>;
  selectedItem: Ref;
  getItems: Function;
  selectItem: Function;
};

export const useMentionItem = ({ search, props, items, selectedItem, getItems, selectItem }: UseMentionItem) => {
  const onKeyDown = async (e: KeyboardEvent) => {
    e.preventDefault();
    if (e.key === 'Backspace') {
      if (search.value === '') {
        props.editor?.chain().focus().deleteRange(props.range).run();
        return;
      }
      search.value = search.value.slice(0, search.value.length - 1);
      props.editor
        ?.chain()
        .focus()
        .deleteRange({ from: props.range?.to - 1, to: props.range?.to })
        .run();
      items.value = await getItems();
      return;
    }
    if (e.key === 'Enter' && selectedItem.value) selectItem(selectedItem.value);
    if (e.key === 'ArrowDown') {
      let currentItemIndex = items.value.findIndex((item) => item.id === selectedItem.value.id);
      if (currentItemIndex === -1 || currentItemIndex === items.value.length - 1) currentItemIndex = -1;
      selectedItem.value = items.value.find((_, index) => index === currentItemIndex + 1);
    }
    if (e.key === 'ArrowUp') {
      let currentItemIndex = items.value.findIndex((item) => item.id === selectedItem.value.id);
      if (currentItemIndex === 0 || currentItemIndex === -1) currentItemIndex = items.value.length;
      selectedItem.value = items.value.find((_, index) => index === currentItemIndex - 1);
    }

    if (validKeyCodes.includes(e.keyCode)) {
      search.value = search.value + e.key;
      props.editor?.chain().focus().insertContent(e.key).run();
      items.value = await getItems();
      selectedItem.value = items.value[0];
    }
  };

  return { onKeyDown };
};
