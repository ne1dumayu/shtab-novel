import { ref } from 'vue';

export const userStore = () => {
  const userProfile = ref({});

  const setData = ({ id, token, taskId, teamId }) => {
    userProfile.value = {
      id,
      token,
      taskId,
      teamId,
    };
  };

  return { userProfile, setData };
};
