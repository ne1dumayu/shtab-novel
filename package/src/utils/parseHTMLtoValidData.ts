const setAlignAttribute = (checkedNode: Element, nodeToSetAttribute: Element) => {
  if (checkedNode.classList.contains('fr-fir')) {
    nodeToSetAttribute.setAttribute('align', 'right');
  } else if (checkedNode.classList.contains('fr-fil')) {
    nodeToSetAttribute.setAttribute('align', 'left');
  } else {
    nodeToSetAttribute.setAttribute('align', 'center');
  }
};

const moveImages = (img: Element) => {
  const currentAlign = img.getAttribute('align');
  if (!currentAlign) {
    setAlignAttribute(img, img);
  }
  const currentWidth = img.getAttribute('width');
  if (!currentWidth && img.hasAttribute('style')) {
    const widthStyle = (img as HTMLElement).style.width;
    if (widthStyle) {
      img.setAttribute('width', widthStyle);
    }
  }

  const parent = img.parentNode as HTMLElement;
  parent.parentNode?.insertBefore(img, parent);
  parent.remove();
};

const extractFigcaption = (imgWrapper: Element) => {
  const img = imgWrapper.querySelector('img');
  const figcaption = imgWrapper.querySelector('.fr-inner');
  if (figcaption) {
    img?.setAttribute('figcaption', figcaption.textContent?.trim() || '');
  }
  img && setAlignAttribute(imgWrapper, img);
  if (imgWrapper.hasAttribute('style')) {
    const widthStyle = (imgWrapper as HTMLElement).style.width;
    if (widthStyle && img) {
      img.setAttribute('width', widthStyle);
    }
  }

  img && imgWrapper.parentNode?.insertBefore(img, imgWrapper);
  imgWrapper.remove();
};

const extractHighlightedText = (span: Element) => {
  const bgColorStyle = (span as HTMLElement).style.backgroundColor;
  const text = span.textContent?.trim() || '';
  if (text && bgColorStyle) {
    const mark = document.createElement('mark');
    mark.textContent = text;
    mark.setAttribute('data-color', bgColorStyle);
    mark.setAttribute('style', `background-color: ${bgColorStyle}; color: inherit`);
    const parent = span.parentNode as HTMLElement;
    parent.replaceChild(mark, span);
  }
};

const replaceMentionUser = (mention: Element) => {
  const mentionItem = document.createElement('mention-item');
  mentionItem.setAttribute('data-id', mention.getAttribute('data-id') || '');
  mentionItem.setAttribute('data-label', mention.textContent || '');
  mention.replaceWith(mentionItem);
};

const replaceTributeTasks = (mention: Element | HTMLAnchorElement) => {
  const mentionItem = document.createElement('mention-item');
  if (mention instanceof HTMLAnchorElement) {
    const taskId = mention.href.split('/').pop() || '';
    mentionItem.setAttribute('data-id', taskId);
    mentionItem.setAttribute('data-label', mention.textContent || '');
    mentionItem.setAttribute('href', mention.href);
  }
  mention.replaceWith(mentionItem);
};

export const parseHTMLtoValidData = (html: string) => {
  const parser = new DOMParser();
  const doc = parser.parseFromString(html, 'text/html');

  doc.querySelectorAll('span.fr-img-caption').forEach(extractFigcaption);

  doc.querySelectorAll('span.fr-tribute-user').forEach(replaceMentionUser);

  doc.querySelectorAll('a.fr-tribute-tasks').forEach(replaceTributeTasks);

  doc.querySelectorAll('p > img').forEach(moveImages);

  ['h1', 'h2', 'h3'].forEach((tag) => {
    doc.querySelectorAll(`${tag} > img`).forEach(moveImages);
  });

  doc.querySelectorAll('span').forEach((span) => {
    const bgColorStyle = (span as HTMLElement).style.backgroundColor;
    if (bgColorStyle) {
      extractHighlightedText(span);
    }
  });

  doc.querySelectorAll('td').forEach((td) => {
    const style = td.getAttribute('style');
    if (style && style.includes('background-color')) {
      const bgColorRegex = /background-color:\s*([^;]+);/;
      const match = style.match(bgColorRegex);
      if (match) {
        const bgColor = match[1];
        td.removeAttribute('style');
        td.setAttribute('data-background-color', bgColor);
      }
    }
  });

  return doc.body.innerHTML;
};
