const debounce = (fn: (...args: any[]) => void, ms: number = 500) => {
  let timeout: undefined | number;

  return (...args: any[]) => {
    clearTimeout(timeout);
    timeout = setTimeout(() => fn(...args), ms);
  };
};

export default debounce;
